#include "filecode.h"

#include <fstream>

void readCodeFiles(string sourcePath, DatarefDatabase* data)
{
    // Read the source
    ifstream sourceReadStream(sourcePath);
    if(sourceReadStream)
    {
        string varGetter = "";
        string varSetter = "";
        int typeID = 0;

        string accessor = "";

        string line;
        while(getline(sourceReadStream, line))
        {
            if("" == varGetter && "" == varSetter)
            {
                varGetter = getGetterVarName(line).varName;
                varSetter = getSetterVarName(line).varName;
                if("" != varGetter) { typeID = getGetterVarName(line).type; }
                if("" != varSetter) { typeID = getSetterVarName(line).type; }
            }
            else if("}" == line)
            {
                if("" == varGetter)
                {
                    data->setSetter(varSetter, accessor, typeID);
                }
                else
                {
                    data->setGetter(varGetter, accessor, typeID);
                }
                varGetter = "";
                varSetter = "";
                accessor = "";
            }
            else if("{" == line)
            {
                continue;
            }
            else
            {
                if(accessor != "") { accessor += "\n"; }
                accessor += line;
            }
        }
    }
}

void writeCodeFiles(string sourcePath, DatarefDatabase* data)
{
    // Write the header
    string headerPath = sourcePath.substr(0, sourcePath.size()-3) + "h";
    ofstream headerStream(headerPath);
    if(headerStream)
    {
        headerStream << "#ifndef DATAREFS_H" << endl
                     << "#define DATAREFS_H" << endl << endl;

        headerStream << "// Created using DatarefGenerator" << endl << endl;

        headerStream << "struct CustomDatarefs {" << endl;
        for(size_t i=0;i<data->size();i++)
        {
            string type;
            if(data->at(i)->isData()) // Handle types priority
            {
                type = datarefTypeVariable[5];
            }
            else if(data->at(i)->isFloatArray())
            {
                type = datarefTypeVariable[3];
            }
            else if(data->at(i)->isIntArray())
            {
                type = datarefTypeVariable[4];
            }
            else if(data->at(i)->isDouble())
            {
                type = datarefTypeVariable[2];
            }
            else if(data->at(i)->isFloat())
            {
                type = datarefTypeVariable[1];
            }
            else if(data->at(i)->isInt())
            {
                type = datarefTypeVariable[0];
            }
            if(type != "")
            {
                bool isArray = data->at(i)->isFloatArray() || data->at(i)->isIntArray();
                headerStream << "\t" << type << " " << data->at(i)->getVarName();
                if(isArray) { headerStream << "[1]"; }
                headerStream << ";" << endl;
            }
        }
        headerStream << "};" << endl << "extern CustomDatarefs datarefs;" << endl << endl;

        headerStream << "void registerDatarefs();" << endl
                     << "void unregisterDatarefs();" << endl << endl;

        headerStream << "// Datarefs accessors" << endl;
        for(size_t i=0;i<data->size();i++)
        {
            // getter
            string varName = data->at(i)->getVarName();
            for(int j=0;j<numberOfDatarefTypes;j++)
            {
                if(data->at(i)->isType(j))
                {
                    headerStream << datarefTypeVariable[j] << " getter" << datarefTypeNames[j] <<"_" << varName << "(void* inRefcon";
                    if(3 == j) { headerStream << ", float* outValues, int inOffset, int inMax"; }
                    if(4 == j) { headerStream << ", int* outValues, int inOffset, int inMax"; }
                    if(5 == j) { headerStream << ", void* outValue, int inOffset, int inMaxLength"; }
                    headerStream << ");" << endl;
                }
            }

            // setter
            for(int j=0;j<numberOfDatarefTypes;j++)
            {
                if(data->at(i)->isType(j))
                {
                    headerStream << "void setter" << datarefTypeNames[j] <<"_" << varName << "(void* inRefcon, ";
                    switch(j)
                    {
                    case 0:
                        headerStream << "int value";
                        break;
                    case 1:
                        headerStream << "float value";
                        break;
                    case 2:
                        headerStream << "double value";
                        break;
                    case 3:
                        headerStream << "float* inValues, int inOffset, int inCount";
                        break;
                    case 4:
                        headerStream << "int* inValues, int inOffset, int inCount";
                        break;
                    case 5:
                        headerStream << "void* inValue, int inOffset, int inLength";
                        break;
                    }
                    headerStream << ");" << endl;
                }
            }
            headerStream << endl;
        }
        headerStream << "#endif // DATAREFS_H";
    }

    // Write the source
    ofstream sourceStream(sourcePath);
    if(sourceStream)
    {
        size_t i=headerPath.size()-1;
        string headerName = "";
        bool gotHeaderName = false;
        while(!gotHeaderName)
        {
            if(headerPath[i] == '/' || headerPath[i] == '\\')
            {
                gotHeaderName = true;
            }
            else
            {
                headerName = headerPath[i] + headerName;
            }
            if(0==i)
            {
                gotHeaderName = true;
            }
            else
            {
                i--;
            }
        }
        sourceStream << "#include \"" << headerName << "\"" << endl << endl;

        sourceStream << "#include <vector>" << endl
                     << "#include \"XPLMDataAccess.h\"" << endl << endl;

        sourceStream << "static std::vector<XPLMDataRef> datarefsVect;" << endl << endl;

        sourceStream << "CustomDatarefs datarefs;" << endl << endl;

        sourceStream << "void registerDatarefs()" << endl << "{" << endl;
        for(size_t i=0;i<data->size();i++)
        {
            if(data->at(i)->getType() != 0)
            {
                sourceStream << "\tdatarefsVect.push_back(XPLMRegisterDataAccessor(\""
                             << data->at(i)->getID() << "\", "
                             << data->at(i)->getType() << ", "
                             << ((data->at(i)->isWritable()) ? "1, " : "0, ");

                string varName = data->at(i)->getVarName();

                for(int j=0;j<numberOfDatarefTypes;j++)
                {
                    if(data->at(i)->isType(j))
                    {
                        sourceStream << "getter" << datarefTypeNames[j] << "_" << varName << ", ";
                        if(data->at(i)->isWritable())
                        {
                            sourceStream << "setter" << datarefTypeNames[j] << "_" << varName << ", ";
                        } else { sourceStream << "nullptr, "; }
                    }
                    else
                    {
                        sourceStream << "nullptr, nullptr, ";
                    }
                }
                sourceStream << "nullptr, nullptr));" << endl;

                sourceStream << "\tdatarefs." << varName << " = ";
                if(data->at(i)->isData()) // Handle types priority
                {
                    sourceStream << "nullptr";
                }
                else if(data->at(i)->isFloatArray())
                {
                    sourceStream << "{0.f}";
                }
                else if(data->at(i)->isIntArray())
                {
                    sourceStream << "{0}";
                }
                else if(data->at(i)->isDouble())
                {
                    sourceStream << "0.";
                }
                else if(data->at(i)->isFloat())
                {
                    sourceStream << "0.f";
                }
                else if(data->at(i)->isInt())
                {
                    sourceStream << "0";
                }
                sourceStream << ";" << endl;
            }
        }
        sourceStream << "}" << endl << endl;


        sourceStream << "void unregisterDatarefs()" << endl << "{" << endl;
        sourceStream << "\tfor(size_t i=0;i<datarefsVect.size();i++)" << endl
                     << "\t{" << endl
                     << "\t\tXPLMUnregisterDataAccessor(datarefsVect.at(i));" << endl
                     << "\t}" << endl << "}" << endl << endl;

        sourceStream << "// Datarefs accessors" << endl;
        for(size_t i=0;i<data->size();i++)
        {
            string varName = data->at(i)->getVarName();
            for(int j=0;j<numberOfDatarefTypes;j++)
            {
                if(data->at(i)->isType(j))
                {
                    // getter
                    sourceStream << datarefTypeVariable[j] << " getter" << datarefTypeNames[j] << "_" << varName << "(void* inRefcon";
                    if(3 == j) { sourceStream << ", float* outValues, int inOffset, int inMax"; }
                    if(4 == j) { sourceStream << ", int* outValues, int inOffset, int inMax"; }
                    if(5 == j) { sourceStream << ", void* outValue, int inOffset, int inMaxLength"; }
                    sourceStream << ")" << endl << "{" << endl;
                    string getter = data->at(i)->getGetter(j);
                    if(getter != "")
                    {
                        sourceStream << getter;
                    }
                    else if(j<3 && data->at(i)->getTypesVector().size() == 1)
                    {
                        sourceStream << "\t(void) inRefcon;" << endl
                                     << "\treturn datarefs." << varName << ";";
                    }
                    else if(3 == j)
                    {
                        sourceStream << "\treturn 0.f;";
                    }
                    else if(4 == j)
                    {
                        sourceStream << "\treturn 0;";
                    }
                    else if(5 == j)
                    {
                        sourceStream << "\treturn nullptr;";
                    }
                    else
                    {
                        sourceStream << "\treturn NULL;";
                    }
                    sourceStream << endl << "}" << endl << endl;

                    // setter
                    if(data->at(i)->isWritable())
                    {
                        sourceStream << "void setter" << datarefTypeNames[j] <<"_" << varName
                                     << "(void* inRefcon, ";
                        switch(j)
                        {
                        case 0:
                            sourceStream << "int value";
                            break;
                        case 1:
                            sourceStream << "float value";
                            break;
                        case 2:
                            sourceStream << "double value";
                            break;
                        case 3:
                            sourceStream << "float* inValues, int inOffset, int inCount";
                            break;
                        case 4:
                            sourceStream << "int* inValues, int inOffset, int inCount";
                            break;
                        case 5:
                            sourceStream << "void* inValue, int inOffset, int inLength";
                            break;
                        }
                        sourceStream << ")" << endl << "{" << endl;
                        string setter = data->at(i)->getSetter(j);
                        if(setter != "")
                        {
                            sourceStream << setter;
                        }
                        else if(j<3 && data->at(i)->getTypesVector().size() == 1)
                        {
                            sourceStream << "\t(void) inRefcon;" << endl
                                         << "\tdatarefs." << varName << " = value;" << endl;
                        }
                        else if(3 == j)
                        {
                            sourceStream << "\t(void) inRefcon;" << endl
                                         << "\t(void) inValues;" << endl
                                         << "\t(void) inOffset;" << endl
                                         << "\t(void) inCount;";
                        }
                        else if(4 == j)
                        {
                            sourceStream << "\t(void) inRefcon;" << endl
                                         << "\t(void) inValues;" << endl
                                         << "\t(void) inOffset;" << endl
                                         << "\t(void) inCount;";
                        }
                        else if(5 == j)
                        {
                            sourceStream << "\t(void) inRefcon;" << endl
                                         << "\t(void) inValue;" << endl
                                         << "\t(void) inOffset;" << endl
                                         << "\t(void) inLength;";
                        }
                        else
                        {
                            sourceStream << "return;";
                        }
                        sourceStream << endl << "}" << endl << endl;
                    }
                }
            }
        }
    }
}

AccessorData getGetterVarName(string line)
{
    AccessorData result = { "", 0 };
    for(int j=0;j<numberOfDatarefTypes;j++)
    {
        if (line.find(" getter" + datarefTypeNames[j] + "_") != std::string::npos && line.find("(") != std::string::npos)
        {
            size_t underscPos = line.find("_");
            result = {line.substr(underscPos+1, line.find("(")-underscPos-1), j};
            return result;
        }
    }
    return result;
}

AccessorData getSetterVarName(string line)
{
    AccessorData result = { "", 0 };
    for(int j=0;j<numberOfDatarefTypes;j++)
    {
        if (line.find(" setter" + datarefTypeNames[j] + "_") != std::string::npos && line.find("(") != std::string::npos)
        {
            size_t underscPos = line.find("_");
            result = {line.substr(underscPos+1, line.find("(")-underscPos-1), j};
            return result;
        }
    }
    return result;
}
