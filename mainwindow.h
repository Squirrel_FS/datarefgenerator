#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringListModel>

#include "datarefdatabase.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void redrawDatarefList();
    int getIndexSelected();

    void select(int index);

    int calculateTypes();
    void applyTypes(int types);
public slots:
    void addNew();
    void deleteSelected();
    void moveUp();
    void moveDown();

    void displayProperties();

    void varNameModifColor();
    void idModifColor();
    void resetColorVarName();
    void resetColorID();

    void modifyVarName();
    void modifyID();
    void modifyWritable();
    void modifyType();

    void openDatabase();
    void saveDatabase();
    void loadDatabase();

    void writeCode();
    void importCSV();

    void closeEvent(QCloseEvent *event);
private:
    Ui::MainWindow *ui;
    DatarefDatabase datarefDatabase;
    QStringListModel* model;

    QString databasePath;

    bool databaseOpened = false;
};
#endif // MAINWINDOW_H
