#include "dictionary.h"

Dictionary::Dictionary()
{

}

vector<string>::size_type Dictionary::size() const
{
    return keys.size();
}

string Dictionary::get(string key) const
{
    for(vector<string>::size_type i=0; i<keys.size(); i++)
    {
        if(keys.at(i) == key)
        {
            return values.at(i);
        }
    }
    return "";
}

string Dictionary::keyAt(vector<string>::size_type i) const
{
    return keys.at(i);
}

string Dictionary::valueAt(vector<string>::size_type i) const
{
    return values.at(i);
}

void Dictionary::set(string key, string value)
{
    // if already exists then modifies the value
    for(vector<string>::size_type i=0; i<keys.size(); i++)
    {
        if(keys.at(i) == key)
        {
            values.at(i) = value;
            return;
        }
    }
    // if does not exists yet then add the key and value at the end
    keys.push_back(key);
    values.push_back(value);
}

string Dictionary::remove(string key)
{
    string value;
    for(vector<string>::size_type i=0; i<keys.size(); i++)
    {
        if(keys.at(i) == key)
        {
            value = values.at(i);
            keys.erase(keys.begin()+static_cast<long>(i));
            values.erase(keys.begin()+static_cast<long>(i));
            return value;
        }
    }
    return "";
}

void Dictionary::clear()
{
    keys.clear();
    keys.shrink_to_fit();

    values.clear();
    values.shrink_to_fit();
}
