#ifndef DATAREF_H
#define DATAREF_H

#include <string>
#include <vector>

using namespace std;

const int numberOfDatarefTypes = 6;
const string datarefTypeVariable[numberOfDatarefTypes] = {"int", "float", "double", "float", "int", "void*"};
const string datarefTypeNames[numberOfDatarefTypes] = {"Int", "Float", "Double", "FloatArray", "IntArray", "Data"};

class Dataref
{
private:
    string varName = "";
    string id = "";
    bool writable;
    int type;

    string getter[numberOfDatarefTypes];
    string setter[numberOfDatarefTypes];
public:
    Dataref(string varName, string id, bool writable, int type);

    string getVarName();
    string getID();
    bool isWritable();

    int getType();
    vector<int> getTypesVector();
    bool isType(int ref);

    bool isInt();
    bool isFloat();
    bool isDouble();
    bool isFloatArray();
    bool isIntArray();
    bool isData();

    void setVarName(string newVarName);
    void setID(string newID);
    void setWritable(bool newWrittable);
    void setType(int newType);

    string getGetter(int i);
    string getSetter(int i);

    void setGetter(string code, int i);
    void setSetter(string code, int i);
};

#endif // DATAREF_H
