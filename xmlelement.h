#ifndef XMLELEMENT_H
#define XMLELEMENT_H

#include "dictionary.h"

#include <string>
#include <vector>

using namespace std;

class XmlElement
{
private:
    string tag;
    Dictionary attributes;
    vector<XmlElement*> children;
public:
    XmlElement(string tag);
    ~XmlElement();

    void clear();

    string getTag() const;
    void setTag(string tag);

    string getAttributeValue(string attribute) const; // returns "" if not found
    void addAttribute(string attribute, string value);
    string printAttributes() const;

    vector<XmlElement*> getChildren() const;
    void addChild(XmlElement* child);

    string printElement() const;
    void writeFile(string path) const;

    void setFromTextLine(string line);
    void readFile(string path);
};

string tabulate(string xmlText);

string isClosingXmlLine(string line);
bool isSingleLineXmlEntry(string line);

#endif // XMLELEMENT_H
