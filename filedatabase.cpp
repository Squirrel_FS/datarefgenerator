#include "filedatabase.h"

#include <QMessageBox>
#include <QString>

#include "xmlelement.h"

void saveDatabaseFiles(string path, DatarefDatabase* data)
{
    XmlElement xmlDatabase("datarefs");

    for(size_t i=0; i<data->size(); i++)
    {
        XmlElement* dataref = new XmlElement(data->at(i)->getVarName());
        xmlDatabase.addChild(dataref);
        dataref->addAttribute("id", data->at(i)->getID());
        if(data->at(i)->isWritable())
        {
            dataref->addAttribute("writable", "true");
        }
        else
        {
            dataref->addAttribute("writable", "false");
        }
        dataref->addAttribute("type", std::to_string(data->at(i)->getType()));
    }

    xmlDatabase.writeFile(path);
}

void readDatabaseFiles(string path, DatarefDatabase* data)
{
    XmlElement xmlDatabase("datarefs");
    xmlDatabase.readFile(path);

    vector<XmlElement*> datarefs = xmlDatabase.getChildren();
    data->clear();

    for(size_t i=0;i<datarefs.size(); i++)
    {
        string varName = datarefs.at(i)->getTag();
        string id = datarefs.at(i)->getAttributeValue("id");
        bool writable = datarefs.at(i)->getAttributeValue("writable") == "true";
        string typeStr = datarefs.at(i)->getAttributeValue("type");
        Dataref dataref(varName, id, writable, std::stoi(typeStr));
        data->addExisting(dataref);
    }
}
