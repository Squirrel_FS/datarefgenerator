#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <string>
#include <vector>

using namespace std;

class Dictionary
{
private:
    vector<string> keys;
    vector<string> values;
public:
    Dictionary();

    vector<string>::size_type size() const;

    string get(string key) const;
    string keyAt(vector<string>::size_type i) const;
    string valueAt(vector<string>::size_type i) const;

    void set(string key, string value); // adds or modifies

    string remove(string key); // returns the value
    void clear();
};

#endif // DICTIONARY_H
