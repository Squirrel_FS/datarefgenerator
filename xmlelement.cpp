#include "xmlelement.h"

#include <fstream>

XmlElement::XmlElement(string tag)
{
    this->tag = tag;
}

XmlElement::~XmlElement()
{
    for(vector<XmlElement*>::size_type i=0; i<children.size(); i++)
    {
        delete children.at(i);
    }
    children.clear();
}

void XmlElement::clear()
{
    tag = "";
    attributes.clear();
    for(vector<XmlElement*>::size_type i=0; i<children.size(); i++)
    {
        delete children.at(i);
    }
    children.clear();
}

// Tag -------------------------------------------------------------------------

string XmlElement::getTag() const
{
    return tag;
}

void XmlElement::setTag(string tag)
{
    this->tag = tag;
}

// Attributes ------------------------------------------------------------------

string XmlElement::getAttributeValue(string attribute) const // returns "" if not found
{
    return attributes.get(attribute);
}

void XmlElement::addAttribute(string attribute, string value)
{
    attributes.set(attribute, value);
}

string XmlElement::printAttributes() const
{
    if(attributes.size()>0)
    {
        string print;
        for(vector<string>::size_type i=0; i<attributes.size(); i++)
        {
            print += " " + attributes.keyAt(i) + "=\"" + attributes.valueAt(i) + "\"";
        }
        return print;
    }
    return "";
}

// Children --------------------------------------------------------------------

vector<XmlElement*> XmlElement::getChildren() const
{
    return children;
}

void XmlElement::addChild(XmlElement* child)
{
    // check if does not exists yet
    for(vector<XmlElement*>::size_type i=0; i<children.size(); i++)
    {
        if(children.at(i) == child)
        {
            return;
        }
    }
    // add the new child at the end
    children.push_back(child);
}

// Files - write ---------------------------------------------------------------

string XmlElement::printElement() const
{
    string print;

    if(children.size() > 0)
    {
        print = "<" + tag + this->printAttributes() + ">\n";

        for(vector<XmlElement*>::size_type i=0; i<children.size(); i++)
        {
            print += tabulate(children.at(i)->printElement());
        }

        print += "</" + tag + ">\n";
    }
    else
    {
        print = "<" + tag + this->printAttributes() + " />\n";
    }

    return print;
}

void XmlElement::writeFile(string path) const
{
    ofstream stream(path.c_str());
    if(stream)
    {
        stream << this->printElement();
        stream.close();
    }
}

// Files - read ----------------------------------------------------------------

void XmlElement::setFromTextLine(string line)
{
    // clear beginning tabulations and <, along ending /> or >
    bool copy = false;
    string clearLine = "";
    for(string::size_type i=0; i<line.size(); i++)
    {
        if(i+1<line.size())
        {
            if(line.at(i)=='/' && line.at(i+1)=='>')
            {
                copy = false;
            }
        }
        if(line.at(i)=='>')
        {
            copy = false;
        }
        if(copy)
        {
            clearLine += line.at(i);
        }
        else if(line.at(i) == '<')
        {
            copy = true;
        }
    }

    string::size_type i = 0;

    // read the tag
    string readTag = "";
    while(i<clearLine.size() &&
          clearLine.at(i)!=' ' && clearLine.at(i)!='/' && clearLine.at(i)!='>')
    {
        readTag += clearLine.at(i);
        i++;
    }
    this->setTag(readTag);

    // skip space(s)
    while(i<clearLine.size() && clearLine.at(i)==' ') { i++; }

    // add attributes
    while(i<clearLine.size() &&
          clearLine.at(i)!='/' && clearLine.at(i)!='>')
    {
        string key = "";
        string value = "";

        // key
        while(i<clearLine.size() &&
              clearLine.at(i)!='=' && clearLine.at(i)!='/' && clearLine.at(i)!='>')
        {
            key += clearLine.at(i);
            i++;
        }

        // skip equal, spaces and quotation marks
        while(i<clearLine.size() &&
              (clearLine.at(i)=='=' || clearLine.at(i)==' ' || clearLine.at(i)=='\"'))
        { i++; }

        // value
        while(i<clearLine.size() &&
              clearLine.at(i)!='\"')
        {
            value += clearLine.at(i);
            i++;
        }

        // skip equal, spaces and quotation marks
        while(i<clearLine.size() &&
              (clearLine.at(i)=='=' || clearLine.at(i)==' ' || clearLine.at(i)=='\"'))
        { i++; }

        attributes.set(key, value);
    }
}

void XmlElement::readFile(string path)
{
    ifstream stream(path);

    if(stream)
    {
        this->clear();

        string line;
        getline(stream, line);
        this->setFromTextLine(line);

        // Tag stack
        vector<XmlElement*> stack;
        stack.push_back(this);

        while(getline(stream, line))
        {
            string closingTag = isClosingXmlLine(line);
            if(closingTag == "") // opening
            {
                XmlElement* child_p = new XmlElement("");
                child_p->setFromTextLine(line);
                stack.back()->addChild(child_p);
                stack.push_back(child_p);
            }
            else if(isSingleLineXmlEntry(line)) // single line
            {
                XmlElement* child_p = new XmlElement("");
                child_p->setFromTextLine(line);
                stack.back()->addChild(child_p);
            }
            else // closing
            {
                stack.pop_back();
            }
        }

        stack.clear();
        stream.close();
    }
}

// Tools -----------------------------------------------------------------------

string tabulate(string xmlText)
{
    string text = "\t";
    for(string::size_type i=0; i<xmlText.size(); i++)
    {
        text += xmlText.at(i);
        if(xmlText.at(i) == '\n' && i!=xmlText.size()-1)
        {
            text += '\t';
        }
    }
    return text;
}

string isClosingXmlLine(string line)
{
    bool isClosing = false;

    // at the end
    if(line.at(line.size()-2)=='/')
    {
        isClosing = true;
    }

    string::size_type i=0;
    while((line.at(i)==' ' || line.at(i)=='\t') && i<line.size())
    {
        i++;
    }

    if(i+1<line.size())
    {
        if(line.at(i)=='<')
        {
            if(line.at(i+1)=='/')
            {
                isClosing = true;
                i++;
            }
            i++;
        }
    }

    string tag = "";
    while(line.at(i)!=' ' && line.at(i)!='/' && line.at(i)!='>' && i<line.size())
    {
        tag += line.at(i);
        i++;
    }

    if(isClosing)
    {
       return tag;
    }
    else
    {
        return "";
    }
}

bool isSingleLineXmlEntry(string line)
{
    if(line.size()>2)
    {
        return line.at(line.size()-2) == '/';
    }
    return false;
}
