#ifndef FILECODE_H
#define FILECODE_H

#include <string>

#include "datarefdatabase.h"

struct AccessorData{
    string varName;
    int type;
};

void readCodeFiles(string sourcePath, DatarefDatabase* data);
void writeCodeFiles(string sourcePath, DatarefDatabase* data);

AccessorData getGetterVarName(string line);
AccessorData getSetterVarName(string line);

#endif // FILECODE_H
