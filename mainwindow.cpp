#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialogprojects.h"

#include <QString>
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QCloseEvent>
#include <QTimer>

#include "dataref.h"
#include "filedatabase.h"
#include "filecode.h"
#include "filecsvimport.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    model = new QStringListModel();
    ui->listView->setModel(model);

    displayProperties();

    QObject::connect(ui->button_add, SIGNAL(clicked()), this, SLOT(addNew()));
    QObject::connect(ui->button_remove, SIGNAL(clicked()), this, SLOT(deleteSelected()));
    QObject::connect(ui->button_up, SIGNAL(clicked()), this, SLOT(moveUp()));
    QObject::connect(ui->button_down, SIGNAL(clicked()), this, SLOT(moveDown()));

    QObject::connect(ui->listView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(displayProperties()));

    QObject::connect(ui->variableNameLineEdit, SIGNAL(textEdited(QString)), this, SLOT(varNameModifColor()));
    QObject::connect(ui->variableNameLineEdit, SIGNAL(returnPressed()), this, SLOT(modifyVarName()));
    QObject::connect(ui->iDLineEdit, SIGNAL(textEdited(QString)), this, SLOT(idModifColor()));
    QObject::connect(ui->iDLineEdit, SIGNAL(returnPressed()), this, SLOT(modifyID()));
    QObject::connect(ui->writableCheckBox, SIGNAL(stateChanged(int)), this, SLOT(modifyWritable()));

    QObject::connect(ui->typeIntCheckBox, SIGNAL(stateChanged(int)), this, SLOT(modifyType()));
    QObject::connect(ui->typeFloatCheckBox, SIGNAL(stateChanged(int)), this, SLOT(modifyType()));
    QObject::connect(ui->typeDoubleCheckBox, SIGNAL(stateChanged(int)), this, SLOT(modifyType()));
    QObject::connect(ui->typeFloatArrayCheckBox, SIGNAL(stateChanged(int)), this, SLOT(modifyType()));
    QObject::connect(ui->typeIntArrayCheckBox, SIGNAL(stateChanged(int)), this, SLOT(modifyType()));
    QObject::connect(ui->typeDataCheckBox, SIGNAL(stateChanged(int)), this, SLOT(modifyType()));

    QObject::connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(openDatabase()));
    QObject::connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(saveDatabase()));
    QObject::connect(ui->actionExport, SIGNAL(triggered()), this, SLOT(writeCode()));
    QObject::connect(ui->actionCSV_csv, SIGNAL(triggered()), this, SLOT(importCSV()));
    QObject::connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(close()));

    QTimer::singleShot(0, this, SLOT(openDatabase()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::redrawDatarefList()
{
    QStringList linesList;
    for(size_t i=0; i<datarefDatabase.size(); i++)
    {
        linesList << QString::fromStdString(datarefDatabase.at(i)->getVarName());
    }
    model->setStringList(linesList);
    displayProperties();
}

int MainWindow::getIndexSelected()
{
    return ui->listView->selectionModel()->selectedRows().front().row();
}

void MainWindow::select(int index)
{
    QModelIndex modelIndex = model->index(index, 0);
    ui->listView->selectionModel()->setCurrentIndex(modelIndex, QItemSelectionModel::Select );
}

int MainWindow::calculateTypes()
{
    int types = 0;
    if(ui->typeIntCheckBox->isChecked()) { types += 1; }
    if(ui->typeFloatCheckBox->isChecked()) { types += 2; }
    if(ui->typeDoubleCheckBox->isChecked()) { types += 4; }
    if(ui->typeFloatArrayCheckBox->isChecked()) { types += 8; }
    if(ui->typeIntArrayCheckBox->isChecked()) { types += 16; }
    if(ui->typeDataCheckBox->isChecked()) { types += 32; }
    return types;
}

void MainWindow::applyTypes(int types)
{
    types -= 32;
    ui->typeDataCheckBox->setCheckState((types >= 0 ? Qt::CheckState::Checked : Qt::CheckState::Unchecked));
    if(types < 0) { types += 32; }
    types -= 16;
    ui->typeIntArrayCheckBox->setCheckState((types >= 0 ? Qt::CheckState::Checked : Qt::CheckState::Unchecked));
    if(types < 0) { types += 16; }
    types -= 8;
    ui->typeFloatArrayCheckBox->setCheckState((types >= 0 ? Qt::CheckState::Checked : Qt::CheckState::Unchecked));
    if(types < 0) { types += 8; }
    types -= 4;
    ui->typeDoubleCheckBox->setCheckState((types >= 0 ? Qt::CheckState::Checked : Qt::CheckState::Unchecked));
    if(types < 0) { types += 4; }
    types -= 2;
    ui->typeFloatCheckBox->setCheckState((types >= 0 ? Qt::CheckState::Checked : Qt::CheckState::Unchecked));
    if(types < 0) { types += 2; }
    types -= 1;
    ui->typeIntCheckBox->setCheckState((types >= 0 ? Qt::CheckState::Checked : Qt::CheckState::Unchecked));
    if(types < 0) { types += 1; }
}





void MainWindow::addNew()
{
    datarefDatabase.addNew();
    redrawDatarefList();
    int index = int(datarefDatabase.size())-1;
    select(index);
}

void MainWindow::deleteSelected()
{
    if(ui->listView->selectionModel()->hasSelection())
    {
        int index = getIndexSelected();
        QString question = QString::fromStdString("Are you sure to want to delete "+datarefDatabase.at(size_t(index))->getVarName()+"?");
        int result = QMessageBox::warning(this, "Delete selected element", question, QMessageBox::Yes | QMessageBox::No);

        if(result == QMessageBox::Yes)
        {
            datarefDatabase.deleteItem(index);
            redrawDatarefList();
            displayProperties();
        }
    }
}

void MainWindow::moveUp()
{
    if(ui->listView->selectionModel()->hasSelection())
    {
        int index = getIndexSelected();
        datarefDatabase.moveUp(index);
        redrawDatarefList();
        index = max(index-1, 0);
        select(index);
    }
}

void MainWindow::moveDown()
{
    if(ui->listView->selectionModel()->hasSelection())
    {
        int index = getIndexSelected();
        datarefDatabase.moveDown(index);
        redrawDatarefList();
        index = min(index+1, int(datarefDatabase.size())-1);
        select(index);
    }
}




void MainWindow::displayProperties()
{
    if(ui->listView->selectionModel()->hasSelection())
    {
        size_t index = size_t(getIndexSelected());
        ui->variableNameLineEdit->setText(QString::fromStdString(datarefDatabase.at(index)->getVarName()));
        ui->variableNameLineEdit->setEnabled(true);
        ui->iDLineEdit->setText(QString::fromStdString(datarefDatabase.at(index)->getID()));
        ui->iDLineEdit->setEnabled(true);
        if(datarefDatabase.at(index)->isWritable())
        {
            ui->writableCheckBox->setCheckState(Qt::CheckState::Checked);
        }
        else
        {
            ui->writableCheckBox->setCheckState(Qt::CheckState::Unchecked);
        }
        ui->writableCheckBox->setEnabled(true);

        ui->typeIntCheckBox->setEnabled(true);
        ui->typeFloatCheckBox->setEnabled(true);
        ui->typeDoubleCheckBox->setEnabled(true);
        ui->typeFloatArrayCheckBox->setEnabled(true);
        ui->typeIntArrayCheckBox->setEnabled(true);
        ui->typeDataCheckBox->setEnabled(true);

        applyTypes(datarefDatabase.at(index)->getType());
    }
    else
    {
        ui->variableNameLineEdit->clear();
        ui->variableNameLineEdit->setEnabled(false);
        ui->iDLineEdit->clear();
        ui->iDLineEdit->setEnabled(false);

        ui->writableCheckBox->setEnabled(false);

        ui->typeIntCheckBox->setEnabled(false);
        ui->typeFloatCheckBox->setEnabled(false);
        ui->typeDoubleCheckBox->setEnabled(false);
        ui->typeFloatArrayCheckBox->setEnabled(false);
        ui->typeIntArrayCheckBox->setEnabled(false);
        ui->typeDataCheckBox->setEnabled(false);

        ui->writableCheckBox->setCheckState(Qt::CheckState::Unchecked);

        applyTypes(0);
    }
    resetColorVarName();
    resetColorID();
}



void MainWindow::varNameModifColor()
{
    QPalette *palette = new QPalette();
    palette->setColor(QPalette::Base,Qt::yellow);
    ui->variableNameLineEdit->setPalette(*palette);
}

void MainWindow::idModifColor()
{
    QPalette *palette = new QPalette();
    palette->setColor(QPalette::Base,Qt::yellow);
    ui->iDLineEdit->setPalette(*palette);
}

void MainWindow::resetColorVarName()
{
    QPalette *palette = new QPalette();
    palette->setColor(QPalette::Base,Qt::white);
    ui->variableNameLineEdit->setPalette(*palette);
}

void MainWindow::resetColorID()
{
    QPalette *palette = new QPalette();
    palette->setColor(QPalette::Base,Qt::white);
    ui->iDLineEdit->setPalette(*palette);
}




void MainWindow::modifyVarName()
{
    if(ui->listView->selectionModel()->hasSelection())
    {
        size_t index = size_t(getIndexSelected());
        string varName = ui->variableNameLineEdit->text().toStdString();

        if(!datarefDatabase.varExists(varName))
        {
            datarefDatabase.at(index)->setVarName(varName);

            model->setData(ui->listView->selectionModel()->selectedRows().front(), QString::fromStdString(varName));

            resetColorVarName();
        }
        if(varName == datarefDatabase.at(index)->getVarName())
        {
            resetColorVarName();
        }
    }
}

void MainWindow::modifyID()
{
    if(ui->listView->selectionModel()->hasSelection())
    {
        size_t index = size_t(getIndexSelected());
        string id = ui->iDLineEdit->text().toStdString();

        if(!datarefDatabase.idExists(id))
        {
            datarefDatabase.at(index)->setID(id);

            resetColorID();
        }
        if(id == datarefDatabase.at(index)->getID())
        {
            resetColorID();
        }
    }
}

void MainWindow::modifyWritable()
{
    if(ui->listView->selectionModel()->hasSelection())
    {
        size_t index = size_t(getIndexSelected());
        datarefDatabase.at(index)->setWritable(ui->writableCheckBox->isChecked());
    }
}

void MainWindow::modifyType()
{
    if(ui->listView->selectionModel()->hasSelection())
    {
        size_t index = size_t(getIndexSelected());
        datarefDatabase.at(index)->setType(calculateTypes());
    }
}



void MainWindow::openDatabase()
{
    if(databaseOpened)
    {
        this->saveDatabase();
    }

    DialogProjects dialogProjects(this);
    dialogProjects.exec();

    QString selectedDBPath = dialogProjects.getProject();
    if("" == selectedDBPath && !databaseOpened)
    {
        this->close();
    }
    else if("" != selectedDBPath)
    {
        databasePath = selectedDBPath;
        loadDatabase();
        redrawDatarefList();
        // Get Getter and Setter before it's too late (name changed)
        std::string path = databasePath.toStdString();
        std::string codePath = path.substr(0, path.size()-3) + "cpp";
        readCodeFiles(codePath, &datarefDatabase);

        databaseOpened = true;
    }
}

void MainWindow::saveDatabase()
{
    QString question = QString::fromStdString("Save? (only 1 backup)");
    int result = QMessageBox::warning(this, "Save", question, QMessageBox::Yes | QMessageBox::No);

    if(result == QMessageBox::Yes)
    {
        QString backup = databasePath.split(".").front() + "_bak.xml";
        QFile::copy(databasePath, backup);
        saveDatabaseFiles(databasePath.toStdString(), &datarefDatabase);
    }
}

void MainWindow::loadDatabase()
{
    readDatabaseFiles(databasePath.toStdString(), &datarefDatabase);
}

void MainWindow::writeCode()
{
    QString question = QString::fromStdString("Write the code?");
    int result = QMessageBox::warning(this, "Write .h and .cpp", question, QMessageBox::Yes | QMessageBox::No);

    if(result == QMessageBox::Yes)
    {
        std::string path = databasePath.toStdString();
        std::string codePath = path.substr(0, path.size()-3) + "cpp";
        readCodeFiles(codePath, &datarefDatabase);
        writeCodeFiles(codePath, &datarefDatabase);
    }
}

void MainWindow::importCSV()
{
    QString csvPath  = QFileDialog::getOpenFileName(this, "Select the csv to import", QDir::currentPath(), "csv (*.csv)");
    if(!csvPath.isEmpty())
    {
        importCsvFile(csvPath.toStdString(), &datarefDatabase);
        redrawDatarefList();
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(databaseOpened)
    {
        QString question = QString::fromStdString("Save before leaving?");
        int result = QMessageBox::question(this, "Save", question, QMessageBox::Yes | QMessageBox::No);

        if(result == QMessageBox::Yes)
        {
            QString backup = databasePath.split(".").front() + "_bak.xml";
            QFile::copy(databasePath, backup);
            saveDatabaseFiles(databasePath.toStdString(), &datarefDatabase);
        }
    }
    event->accept();
}
