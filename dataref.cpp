#include "dataref.h"

Dataref::Dataref(string varName, string id, bool writable, int type)
{
    this->varName = varName;
    this->id = id;
    this->writable = writable;
    this->type = type;
}

string Dataref::getVarName()
{
    return varName;
}

string Dataref::getID()
{
    return id;
}

bool Dataref::isWritable()
{
    return writable;
}
int Dataref::getType()
{
    return type;
}

vector<int> Dataref::getTypesVector()
{
    vector<int> vect;
    if(isInt()) { vect.push_back(0); }
    if(isFloat()) { vect.push_back(1); }
    if(isDouble()) { vect.push_back(2); }
    if(isFloatArray()) { vect.push_back(3); }
    if(isIntArray()) { vect.push_back(4); }
    if(isData()) { vect.push_back(5); }
    return vect;
}

bool Dataref::isType(int ref)
{
    switch(ref)
    {
    case 0:
        return isInt();
    case 1:
        return isFloat();
    case 2:
        return isDouble();
    case 3:
        return isFloatArray();
    case 4:
        return isIntArray();
    case 5:
        return isData();
    default:
        return false;
    }
}

bool Dataref::isInt()
{
    return ((type%2) == 1);
}

bool Dataref::isFloat()
{
    int a = type - (((type - 32) >= 0) ? 32 : 0);
    a = type - (((type - 16) >= 0) ? 16 : 0);
    a = type - (((type - 8) >= 0) ? 8 : 0);
    a = type - (((type - 4) >= 0) ? 4 : 0);
    return (a - 2) >= 0;
}

bool Dataref::isDouble()
{
    int a = type - (((type - 32) >= 0) ? 32 : 0);
    a = type - (((type - 16) >= 0) ? 16 : 0);
    a = type - (((type - 8) >= 0) ? 8 : 0);
    return (a - 4) >= 0;
}

bool Dataref::isFloatArray()
{
    int a = type - (((type - 32) >= 0) ? 32 : 0);
    a = type - (((type - 16) >= 0) ? 16 : 0);
    return (a - 8) >= 0;
}

bool Dataref::isIntArray()
{
    int a = type - (((type - 32) >= 0) ? 32 : 0);
    return (a - 16) >= 0;
}

bool Dataref::isData()
{
    return (type - 32) >= 0;
}


void Dataref::setVarName(string newVarName)
{
    varName = newVarName;
}

void Dataref::setID(string newID)
{
    id = newID;
}

void Dataref::setWritable(bool newWritable)
{
    writable = newWritable;
}

void Dataref::setType(int newType)
{
    type = newType;
}

string Dataref::getGetter(int i)
{
    return getter[i];
}

string Dataref::getSetter(int i)
{
    return setter[i];
}

void Dataref::setGetter(string code, int i)
{
    getter[i] = code;
}

void Dataref::setSetter(string code, int i)
{
    setter[i] = code;
}
