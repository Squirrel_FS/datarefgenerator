#ifndef FILEDATABASE_H
#define FILEDATABASE_H

#include <QWidget>

#include <string>

#include "datarefdatabase.h"

void saveDatabaseFiles(string path, DatarefDatabase* data);

void readDatabaseFiles(string path, DatarefDatabase* data);


#endif // FILEDATABASE_H
