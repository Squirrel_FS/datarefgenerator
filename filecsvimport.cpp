#include "filecsvimport.h"

#include <QString>
#include <QMessageBox>

#include <fstream>

void importCsvFile(string path, DatarefDatabase* data)
{
    ifstream csvStream(path);
    if(csvStream)
    {
        string line;
        getline(csvStream, line); // Titles
        while(getline(csvStream, line))
        {
            string varName = "";
            string id = "";
            bool writable = false;
            string strTypes = "";
            int types = 0;

            unsigned scCounter = 0;
            for(size_t i=0; i<line.size(); i++)
            {
                char c = line[i];
                if(c != ';' && 0==scCounter)
                {
                    varName += c;
                }
                else if(c != ';' && 1==scCounter)
                {
                    id += c;
                }
                else if(c != ';' && 2==scCounter)
                {
                    writable = (c == '1');
                }
                else if(c != ';' && 3==scCounter)
                {
                    strTypes += c;
                }
                else if(scCounter>3)
                {
                    break;
                }
                else
                {
                    scCounter++;
                }
            }

            types = std::stoi(strTypes);

            Dataref dataref = Dataref(varName, id, writable, types);
            data->addExisting(dataref);
        }
    }
}
