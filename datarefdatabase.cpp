#include "datarefdatabase.h"

DatarefDatabase::DatarefDatabase()
{

}

bool DatarefDatabase::varExists(string varName)
{
    for(size_t i=0;i<database.size();i++)
    {
        if(database.at(i).getVarName() == varName) { return true; }
    }
    return false;
}

bool DatarefDatabase::idExists(string id)
{
    for(size_t i=0;i<database.size();i++)
    {
        if(database.at(i).getID() == id) { return true; }
    }
    return false;
}

size_t DatarefDatabase::getIndexID(string id)
{
    for(size_t i=0;i<database.size();i++)
    {
        if(database.at(i).getID() == id) { return i; }
    }
    return string::npos;
}

void DatarefDatabase::setGetter(string varName, string getter, int type)
{
    for(size_t i=0;i<database.size();i++)
    {
        if(database.at(i).getVarName() == varName)
        {
            database.at(i).setGetter(getter, type);
            break;
        }
    }
}

void DatarefDatabase::setSetter(string varName, string setter, int type)
{
    for(size_t i=0;i<database.size();i++)
    {
        if(database.at(i).getVarName() == varName)
        {
            database.at(i).setSetter(setter, type);
            break;
        }
    }
}

void DatarefDatabase::addNew()
{
    int i=0;
    string varName;
    do
    {
        i++;
        varName = "Dataref" + to_string(i);
    }
    while(varExists(varName));

    Dataref newDataref = Dataref(varName, varName, false, 2);
    database.push_back(newDataref);
}

void DatarefDatabase::addExisting(Dataref dataref)
{
    if(!varExists(dataref.getVarName()) && !idExists(dataref.getID()))
    {
        database.push_back(dataref);
    }
    else if(idExists(dataref.getID()))
    {
        // change the name
        size_t pos = getIndexID(dataref.getID());
        database.at(pos).setID(dataref.getID());
        database.at(pos).setWritable(dataref.isWritable());
        database.at(pos).setType(dataref.getType());
    }
    else
    {
        // simple update
        size_t pos = getIndexID(dataref.getID());
        database.at(pos).setWritable(dataref.isWritable());
        database.at(pos).setType(dataref.getType());
    }
}

void DatarefDatabase::deleteItem(int pos)
{
    database.erase(database.begin() + pos);
}

void DatarefDatabase::moveUp(int pos)
{
    if(pos <= 0) { return; }
    iter_swap(database.begin() + pos-1, database.begin() + pos);
}

void DatarefDatabase::moveDown(int pos)
{
    if(size_t(pos+1) >= database.size()) { return; }
    iter_swap(database.begin() + pos, database.begin() + pos+1);
}

size_t DatarefDatabase::size()
{
    return database.size();
}

Dataref* DatarefDatabase::at(size_t i)
{
    return &database.at(i);
}

void DatarefDatabase::clear()
{
    database.clear();
}
