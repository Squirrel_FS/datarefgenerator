#ifndef DIALOGPROJECTS_H
#define DIALOGPROJECTS_H

#include <QDialog>
#include <QString>
#include <QStringListModel>
#include <QFile>

namespace Ui {
class DialogProjects;
}

class DialogProjects : public QDialog
{
    Q_OBJECT

public:
    explicit DialogProjects(QWidget *parent = nullptr);
    ~DialogProjects();
    QString getProject();
public slots:
    void openeSelected();
    void openExisting();
private:
    Ui::DialogProjects *ui;
    QStringListModel* model;
    QStringList projectsList;
    QString selectedDatabase = "";
    QFile projectsListFile;
    void validateChoice();
};

#endif // DIALOGPROJECTS_H
