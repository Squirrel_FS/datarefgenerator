#ifndef FILECSVIMPORT_H
#define FILECSVIMPORT_H

#include <QWidget>

#include <string>

#include "datarefdatabase.h"

void importCsvFile(string path, DatarefDatabase* data);

#endif // FILECSVIMPORT_H
