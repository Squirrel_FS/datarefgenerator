#ifndef DATAREFDATABASE_H
#define DATAREFDATABASE_H

#include <vector>

#include "dataref.h"

using namespace std;

class DatarefDatabase
{
private:
    vector<Dataref> database;
public:
    DatarefDatabase();

    bool varExists(string varName);
    bool idExists(string id);
    size_t getIndexID(string id);

    void setGetter(string varName, string getter, int type);
    void setSetter(string varName, string setter, int type);

    void addNew();
    void addExisting(Dataref dataref);
    void deleteItem(int pos);

    void moveUp(int pos);
    void moveDown(int pos);

    size_t size();
    Dataref* at(size_t i);
    void clear();
};

#endif // DATAREFDATABASE_H
