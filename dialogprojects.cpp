#include "dialogprojects.h"
#include "ui_dialogprojects.h"

#include <QFileDialog>
#include <QTextStream>

DialogProjects::DialogProjects(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogProjects),
    projectsListFile("recent.txt")
{
    ui->setupUi(this);

    QObject::connect(ui->b_openExisting, SIGNAL(clicked()), this, SLOT(openExisting()));
    QObject::connect(ui->b_loadSelected, SIGNAL(clicked()), this, SLOT(openeSelected()));
    QObject::connect(ui->listView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(openeSelected()));

    if(projectsListFile.open(QIODevice::ReadWrite | QIODevice::Text))
    {
         QString fileContent = projectsListFile.readAll();
         projectsList = fileContent.split("\n");
         projectsList.removeAll("");
    }

    model = new QStringListModel();
    ui->listView->setModel(model);
    model->setStringList(projectsList);
}

DialogProjects::~DialogProjects()
{
    delete ui;
}

QString DialogProjects::getProject()
{
    return selectedDatabase;
}

void DialogProjects::openeSelected()
{
    QModelIndexList selection = ui->listView->selectionModel()->selectedRows();
    if(!selection.isEmpty())
    {
        int selected = selection.front().row();
        selectedDatabase = projectsList.at(selected);
        validateChoice();
    }
}

void DialogProjects::openExisting()
{
    selectedDatabase = QFileDialog::getOpenFileName(this, "Select the database", nullptr, "xml (*.xml)");
    validateChoice();
}

void DialogProjects::validateChoice()
{
    if(!selectedDatabase.isEmpty())
    {
        projectsList.removeOne(selectedDatabase);
        projectsList.insert(0, selectedDatabase);
        projectsListFile.reset();
        QTextStream out(&projectsListFile);
        out << projectsList.join("\n");
        projectsListFile.close();

        this->accept();
    }
}
